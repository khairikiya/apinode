var mysql = require('mysql');

//buat koneksi database
const conn = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'projek_node'
});

conn.connect((err) => {
    if (err) throw err;
    console.log('Mysql Terhubung');
});

module.exports = conn;